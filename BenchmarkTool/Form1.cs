﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BenchmarkTool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Run Benchmarks
            var p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.FileName = (@"..\..\..\..\BenchmarkConsoleApp\bin\Release\net5.0\BenchmarkConsoleApp.exe");
            p.Start();
            string output = p.StandardError.ReadToEnd();
            p.WaitForExit();
            int retval = p.ExitCode;

            Console.WriteLine($"\nError stream: {output}");
            if(retval == 1)
            {
                //Move files 
                string benchmarkFixed = "Benchmarks.BenchmarksFixed-report-full.json";
                string benchmarkLeaks = "Benchmarks.BenchmarksLeaks-report-full.json";
                string sourcePath = @"BenchmarkDotNet.Artifacts\results";
                string targetPath = @"..\..\..\BenchmarkResults";
                //Fixed
                string sourceFileFixed = System.IO.Path.Combine(sourcePath, benchmarkFixed);
                benchmarkFixed = "Benchmark.BenchmarksFixed-report-full-" + DateTime.Now.ToFileTime() + ".json"; //Add timestamp to name
                string destFileFixed = System.IO.Path.Combine(targetPath, benchmarkFixed);
                //Leak
                string sourceFileLeak = System.IO.Path.Combine(sourcePath, benchmarkLeaks);
                benchmarkLeaks = "Benchmarks.BenchmarksLeaks-report-full-" + DateTime.Now.ToFileTime() + ".json"; //Add timestamp to name
                string destFileLeak = System.IO.Path.Combine(targetPath, benchmarkLeaks);   //change filename

                System.IO.File.Copy(sourceFileFixed, destFileFixed, true);
                System.IO.File.Copy(sourceFileLeak, destFileLeak, true);
            }
            else
            {
                BenchmarkButton.Text = "Failed";
            }


        }


        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool AllocConsole();

        private void button1_Click_1(object sender, EventArgs e)
        {
            // Load values from the Json file (specific for the BitmapDrawer method)
            Console.WriteLine("knapptryck");
            // Create a class for benchmark results
            //BenchmarkResult result = new BenchmarkResult();
            string jsonBenchmark = System.IO.File.ReadAllText(@"../../../Benchmarks.BenchmarksLeaks-report-full.json");
            JObject jResult = JObject.Parse(jsonBenchmark);

            IList<JToken> benchmarks = jResult["Benchmarks"].ToList();

            IList<Benchmark> benchmarkResults = new List<Benchmark>();
            foreach (JToken result in benchmarks)
            {
                Benchmark benchmarkResult = result.ToObject<Benchmark>();
                benchmarkResults.Add(benchmarkResult);
                Console.WriteLine(benchmarkResult.Method);
            }

            //BenchmarkResults deserializedResult = JsonSerializer.Deserialize<BenchmarkResults>(jsonBenchmark);

            // Prints entire Json file:
            //Console.Write(jsonBenchmark);

            // Deserialize the Json file into an instance of benchmarkresult class
            //JsonSerializer.Deserialize/*<Object type>*/();

        }
    }
}
