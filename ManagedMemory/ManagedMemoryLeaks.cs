﻿using ProfilingLibrary;
using System;

namespace ManagedMemory
{
    public class ManagedMemoryLeaks
    {
        IProfiling _profiler = ProfilingLibraryFactory.GetProfilingLibrary();

        /// <summary>
        /// Measures if managed memory usage for the method RunBitmapDrawer() increases over time.
        /// </summary>
        /// <returns>Returns true if managed memory usage increases over time.</returns>
        public bool MeasureBitmapDrawer()
        {
            bool memGrowth1 = false;
            bool memGrowth2 = false;
            int size = 3;
            long[] results = new long[size];
            //TODO: Testing
            int iteration = 100;
            for (int i = 0; i < size; i++)
            {
                results[i] = BitmapDrawerMemOverIterations(iteration);
                iteration *= 100;
            }
            if (results[0] < results[1]) memGrowth1 = true;
            if (results[1] < results[2]) memGrowth2 = true;
            return memGrowth1 && memGrowth2;
        }
        private long BitmapDrawerMemOverIterations(int iterations)
        {
            long heapSize1 = GC.GetTotalMemory(true);
            for (int i = 0; i < iterations; i++)
            {
                _profiler.RunBitmapDrawer();
            }
            long heapSize2 = GC.GetTotalMemory(true);
            return heapSize2 - heapSize1;
        }

        /// <summary>
        /// Measures if managed memory usage for the method RunCaching() increases over time.
        /// </summary>
        /// <returns>Returns true if managed memory usage increases over time.</returns>
        public bool MeasureCaching()
        {
            bool memGrowth1 = false;
            bool memGrowth2 = false;
            int size = 3;
            long[] results = new long[size];
            //TODO: Testing
            int iteration = 100;
            for (int i = 0; i < size; i++)
            {
                results[i] = CachingMemOverIterations(iteration);
                iteration *= 100;
            }
            if (results[0] < results[1]) memGrowth1 = true;
            if (results[1] < results[2]) memGrowth2 = true;
            return memGrowth1 && memGrowth2;
        }
        private long CachingMemOverIterations(int iterations)
        {
            long heapSize1 = GC.GetTotalMemory(true);
            for (int i = 0; i < iterations; i++)
            {
                _profiler.RunCaching(i);
            }
            long heapSize2 = GC.GetTotalMemory(true);
            return heapSize2 - heapSize1;
        }

        /// <summary>
        /// Measures if managed memory usage for the method RunStaticVariable() increases over time.
        /// </summary>
        /// <returns>Returns true if managed memory usage increases over time.</returns>
        public bool MeasureStaticVariable()
        {
            bool memGrowth1 = false;
            bool memGrowth2 = false;
            int size = 3;
            long[] results = new long[size];
            //TODO: Testing
            int iteration = 100;
            for (int i = 0; i < size; i++)
            {
                results[i] = StaticMemOverIterations(iteration);
                iteration *= 100;
            }
            if (results[0] < results[1]) memGrowth1 = true;
            if (results[1] < results[2]) memGrowth2 = true;
            return memGrowth1 && memGrowth2;
        }
        private long StaticMemOverIterations(int iterations)
        {
            long heapSize1 = GC.GetTotalMemory(true);
            for (int i = 0; i < iterations; i++)
            {
                _profiler.RunStaticVariable();
            }
            long heapSize2 = GC.GetTotalMemory(true);
            return heapSize2 - heapSize1;
        }

        /// <summary>
        /// Measures if managed memory usage for the method RunUnmanagedMemory() increases over time.
        /// </summary>
        /// <returns>Returns true if managed memory usage increases over time.</returns>
        public bool MeasureUnmanagedMemory()
        {
            bool memGrowth1 = false;
            bool memGrowth2 = false;
            int size = 3;
            long[] results = new long[size];
            //TODO: Testing
            int iteration = 100;
            for (int i = 0; i < size; i++)
            {
                results[i] = UnmanagedMemoryMemOverIterations(iteration);
                iteration *= 100;
            }
            if (results[0] < results[1]) memGrowth1 = true;
            if (results[1] < results[2]) memGrowth2 = true;
            return memGrowth1 && memGrowth2;
        }
        private long UnmanagedMemoryMemOverIterations(int iterations)
        {
            long heapSize1 = GC.GetTotalMemory(true);
            for (int i = 0; i < iterations; i++)
            {
                _profiler.RunUnmanagedMemory();
            }
            long heapSize2 = GC.GetTotalMemory(true);
            return heapSize2 - heapSize1;
        }
    }
}
