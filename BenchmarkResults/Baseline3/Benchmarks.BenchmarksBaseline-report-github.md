``` ini

BenchmarkDotNet=v0.13.0, OS=Windows 10.0.19042.1288 (20H2/October2020Update)
Intel Core i7-10750H CPU 2.60GHz, 1 CPU, 12 logical and 6 physical cores
.NET SDK=5.0.400
  [Host]   : .NET 5.0.9 (5.0.921.35908), X64 RyuJIT  [AttachedDebugger]
  ShortRun : .NET Core 5.0.9 (CoreCLR 5.0.921.35908, CoreFX 5.0.921.35908), X64 RyuJIT

Job=ShortRun  IterationCount=3  LaunchCount=1  
WarmupCount=3  

```
|                Method | iterations |             Mean |             Error |           StdDev |    Gen 0 |    Gen 1 |    Gen 2 |   Allocated | Allocated native memory | Native memory leak |
|---------------------- |----------- |-----------------:|------------------:|-----------------:|---------:|---------:|---------:|------------:|------------------------:|-------------------:|
|      **TestBitmapDrawer** |       **1000** |  **44,149,777.8 ns** |  **16,387,520.59 ns** |    **898,255.30 ns** |        **-** |        **-** |        **-** |   **176,000 B** |            **12,624,000 B** |                  **-** |
|           TestCaching |       1000 |     166,524.1 ns |      89,548.57 ns |      4,908.46 ns |  26.1230 |   6.3477 |        - |   166,240 B |                       - |                  - |
|    TestStaticVariable |       1000 |       5,774.7 ns |       1,871.35 ns |        102.58 ns |   3.8223 |        - |        - |    24,000 B |                       - |                  - |
|   TestUnmanagedMemory |       1000 |      70,679.6 ns |      18,011.04 ns |        987.25 ns |   3.7842 |        - |        - |    24,000 B |                10,000 B |                  - |
|            TestBoxing |       1000 |         246.5 ns |          21.90 ns |          1.20 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |       1000 |         244.5 ns |         158.44 ns |          8.68 ns |        - |        - |        - |           - |                       - |                  - |
| TestEventSubscription |       1000 |     864,284.8 ns |   2,150,137.78 ns |    117,856.31 ns |  27.8320 |  10.7422 |   0.4883 |   169,260 B |                   106 B |              106 B |
|      **TestBitmapDrawer** |       **5000** | **224,534,377.8 ns** |  **56,283,455.32 ns** |  **3,085,086.11 ns** |        **-** |        **-** |        **-** |   **880,000 B** |            **63,120,000 B** |                  **-** |
|           TestCaching |       5000 |     955,276.4 ns |     364,756.27 ns |     19,993.52 ns |  89.8438 |  89.8438 |  89.8438 |   771,448 B |                       - |                  - |
|    TestStaticVariable |       5000 |      27,871.1 ns |      11,866.14 ns |        650.42 ns |  19.1040 |        - |        - |   120,000 B |                       - |                  - |
|   TestUnmanagedMemory |       5000 |     367,879.8 ns |     175,049.83 ns |      9,595.07 ns |  19.0430 |        - |        - |   120,000 B |                50,000 B |                  - |
|            TestBoxing |       5000 |       1,216.7 ns |          68.34 ns |          3.75 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |       5000 |       1,228.8 ns |         680.76 ns |         37.31 ns |        - |        - |        - |           - |                       - |                  - |
| TestEventSubscription |       5000 |   4,095,458.1 ns |  35,736,376.82 ns |  1,958,831.40 ns | 140.6250 |  46.8750 |        - |   891,753 B |                   118 B |              118 B |
|      **TestBitmapDrawer** |      **10000** | **458,027,833.3 ns** | **272,499,939.26 ns** | **14,936,641.18 ns** |        **-** |        **-** |        **-** | **1,760,144 B** |           **126,240,000 B** |                  **-** |
|           TestCaching |      10000 |   1,943,821.4 ns |     336,782.84 ns |     18,460.20 ns | 220.7031 | 220.7031 | 220.7031 | 1,582,032 B |                       - |                  - |
|    TestStaticVariable |      10000 |      55,899.9 ns |      13,344.48 ns |        731.46 ns |  38.2080 |        - |        - |   240,000 B |                       - |                  - |
|   TestUnmanagedMemory |      10000 |     759,954.0 ns |     112,643.26 ns |      6,174.36 ns |  38.0859 |        - |        - |   240,000 B |               100,000 B |                  - |
|            TestBoxing |      10000 |       2,439.9 ns |       1,114.55 ns |         61.09 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |      10000 |       2,406.0 ns |         177.26 ns |          9.72 ns |        - |        - |        - |           - |                       - |                  - |
| TestEventSubscription |      10000 |   6,358,118.9 ns |  31,956,432.96 ns |  1,751,639.92 ns | 257.8125 |  93.7500 |        - | 1,782,867 B |                   114 B |              114 B |
