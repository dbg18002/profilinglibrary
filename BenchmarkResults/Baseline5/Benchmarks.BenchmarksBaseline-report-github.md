``` ini

BenchmarkDotNet=v0.13.0, OS=Windows 10.0.19042.1288 (20H2/October2020Update)
Intel Core i7-10750H CPU 2.60GHz, 1 CPU, 12 logical and 6 physical cores
.NET SDK=5.0.400
  [Host]   : .NET 5.0.9 (5.0.921.35908), X64 RyuJIT  [AttachedDebugger]
  ShortRun : .NET Core 5.0.9 (CoreCLR 5.0.921.35908, CoreFX 5.0.921.35908), X64 RyuJIT

Job=ShortRun  IterationCount=3  LaunchCount=1  
WarmupCount=3  

```
|                Method | iterations |             Mean |             Error |           StdDev |    Gen 0 |    Gen 1 |    Gen 2 |   Allocated | Allocated native memory | Native memory leak |
|---------------------- |----------- |-----------------:|------------------:|-----------------:|---------:|---------:|---------:|------------:|------------------------:|-------------------:|
|      **TestBitmapDrawer** |       **1000** |  **44,732,872.2 ns** |  **17,496,590.32 ns** |    **959,047.15 ns** |        **-** |        **-** |        **-** |   **176,000 B** |            **12,624,000 B** |                  **-** |
|           TestCaching |       1000 |     161,940.4 ns |      20,673.61 ns |      1,133.19 ns |  26.1230 |   6.3477 |        - |   166,240 B |                       - |                  - |
|    TestStaticVariable |       1000 |       5,419.6 ns |       3,410.96 ns |        186.97 ns |   3.8223 |        - |        - |    24,000 B |                       - |                  - |
|   TestUnmanagedMemory |       1000 |      48,356.1 ns |      13,134.57 ns |        719.95 ns |   3.7842 |        - |        - |    24,000 B |                10,000 B |           10,000 B |
|            TestBoxing |       1000 |         253.7 ns |          28.46 ns |          1.56 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |       1000 |         253.3 ns |         121.83 ns |          6.68 ns |        - |        - |        - |           - |                       - |                  - |
| TestEventSubscription |       1000 |      52,402.7 ns |      23,682.20 ns |      1,298.10 ns |  24.3530 |   0.1221 |        - |   152,395 B |                   102 B |              102 B |
|      **TestBitmapDrawer** |       **5000** | **227,384,866.7 ns** | **137,945,340.60 ns** |  **7,561,249.59 ns** |        **-** |        **-** |        **-** |   **880,000 B** |            **63,120,000 B** |                  **-** |
|           TestCaching |       5000 |     996,091.1 ns |     120,206.59 ns |      6,588.93 ns |  89.8438 |  89.8438 |  89.8438 |   771,448 B |                       - |                  - |
|    TestStaticVariable |       5000 |      26,794.2 ns |      13,069.40 ns |        716.38 ns |  19.1040 |        - |        - |   120,000 B |                       - |                  - |
|   TestUnmanagedMemory |       5000 |     245,298.0 ns |      78,849.13 ns |      4,321.99 ns |  19.0430 |        - |        - |   120,006 B |                50,000 B |           50,000 B |
|            TestBoxing |       5000 |       1,181.3 ns |          86.01 ns |          4.71 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |       5000 |       1,219.7 ns |         312.49 ns |         17.13 ns |        - |        - |        - |           - |                       - |                  - |
| TestEventSubscription |       5000 |     261,680.0 ns |      16,587.19 ns |        909.20 ns | 121.3379 |   0.2441 |        - |   760,419 B |                   113 B |              113 B |
|      **TestBitmapDrawer** |      **10000** | **439,868,766.7 ns** | **355,352,600.83 ns** | **19,478,075.13 ns** |        **-** |        **-** |        **-** | **1,760,144 B** |           **126,240,000 B** |                  **-** |
|           TestCaching |      10000 |   1,912,438.6 ns |     203,217.68 ns |     11,139.05 ns | 220.7031 | 220.7031 | 220.7031 | 1,582,032 B |                       - |                  - |
|    TestStaticVariable |      10000 |      51,365.7 ns |       2,583.26 ns |        141.60 ns |  38.2080 |        - |        - |   240,000 B |                       - |                  - |
|   TestUnmanagedMemory |      10000 |     487,046.7 ns |      50,168.81 ns |      2,749.92 ns |  38.0859 |        - |        - |   240,000 B |               100,000 B |          100,000 B |
|            TestBoxing |      10000 |       2,381.9 ns |       1,371.39 ns |         75.17 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |      10000 |       2,425.5 ns |         577.85 ns |         31.67 ns |        - |        - |        - |           - |                       - |                  - |
| TestEventSubscription |      10000 |     493,765.2 ns |     233,408.79 ns |     12,793.92 ns | 242.6758 |   0.4883 |        - | 1,520,414 B |                   112 B |              112 B |
