``` ini

BenchmarkDotNet=v0.13.0, OS=Windows 10.0.19042.1288 (20H2/October2020Update)
Intel Core i7-10750H CPU 2.60GHz, 1 CPU, 12 logical and 6 physical cores
.NET SDK=5.0.400
  [Host]   : .NET 5.0.9 (5.0.921.35908), X64 RyuJIT  [AttachedDebugger]
  ShortRun : .NET Core 5.0.9 (CoreCLR 5.0.921.35908, CoreFX 5.0.921.35908), X64 RyuJIT

Job=ShortRun  IterationCount=3  LaunchCount=1  
WarmupCount=3  

```
|                Method | iterations |             Mean |            Error |          StdDev |    Gen 0 |    Gen 1 |    Gen 2 |   Allocated | Allocated native memory | Native memory leak |
|---------------------- |----------- |-----------------:|-----------------:|----------------:|---------:|---------:|---------:|------------:|------------------------:|-------------------:|
|      **TestBitmapDrawer** |       **1000** |  **46,182,588.9 ns** | **44,587,951.76 ns** | **2,444,016.09 ns** |        **-** |        **-** |        **-** |   **176,000 B** |            **12,624,000 B** |                  **-** |
|           TestCaching |       1000 |     164,619.1 ns |     50,901.92 ns |     2,790.11 ns |  26.1230 |   6.3477 |        - |   166,243 B |                       - |                  - |
|    TestStaticVariable |       1000 |       5,297.9 ns |        644.00 ns |        35.30 ns |   3.8223 |        - |        - |    24,000 B |                       - |                  - |
|   TestUnmanagedMemory |       1000 |      73,319.5 ns |      9,782.23 ns |       536.20 ns |   3.7842 |        - |        - |    24,002 B |                10,000 B |                  - |
|            TestBoxing |       1000 |         244.0 ns |         24.33 ns |         1.33 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |       1000 |       2,829.7 ns |        227.52 ns |        12.47 ns |   3.8223 |        - |        - |    24,000 B |                       - |                  - |
| TestEventSubscription |       1000 |      52,444.0 ns |     16,789.68 ns |       920.30 ns |  24.2920 |   0.1221 |        - |   152,390 B |                   102 B |              102 B |
|      **TestBitmapDrawer** |       **5000** | **215,429,966.7 ns** | **19,755,899.93 ns** | **1,082,887.54 ns** |        **-** |        **-** |        **-** |   **880,000 B** |            **63,120,000 B** |                  **-** |
|           TestCaching |       5000 |     967,003.1 ns |    116,788.42 ns |     6,401.57 ns |  90.8203 |  90.8203 |  90.8203 |   771,448 B |                       - |                  - |
|    TestStaticVariable |       5000 |      27,192.4 ns |      6,267.93 ns |       343.57 ns |  19.1040 |        - |        - |   120,000 B |                       - |                  - |
|   TestUnmanagedMemory |       5000 |     357,910.9 ns |     34,021.16 ns |     1,864.81 ns |  19.0430 |        - |        - |   120,000 B |                50,000 B |                  - |
|            TestBoxing |       5000 |       1,269.0 ns |        193.71 ns |        10.62 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |       5000 |      15,309.1 ns |      3,821.63 ns |       209.48 ns |  19.1040 |        - |        - |   120,000 B |                       - |                  - |
| TestEventSubscription |       5000 |     255,357.6 ns |    107,200.33 ns |     5,876.01 ns | 121.3379 |   0.2441 |        - |   760,419 B |                   109 B |              109 B |
|      **TestBitmapDrawer** |      **10000** | **445,010,866.7 ns** | **98,395,060.74 ns** | **5,393,365.30 ns** |        **-** |        **-** |        **-** | **1,760,144 B** |           **126,240,000 B** |                  **-** |
|           TestCaching |      10000 |   1,954,924.7 ns |  1,061,311.52 ns |    58,174.07 ns | 218.7500 | 218.7500 | 218.7500 | 1,582,032 B |                       - |                  - |
|    TestStaticVariable |      10000 |      52,080.5 ns |     23,362.60 ns |     1,280.58 ns |  38.2080 |        - |        - |   240,000 B |                       - |                  - |
|   TestUnmanagedMemory |      10000 |     692,682.3 ns |     50,205.96 ns |     2,751.96 ns |  38.0859 |        - |        - |   240,000 B |               100,000 B |                  - |
|            TestBoxing |      10000 |       2,372.0 ns |        263.74 ns |        14.46 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |      10000 |      29,998.8 ns |     16,374.98 ns |       897.57 ns |  38.2385 |        - |        - |   240,000 B |                       - |                  - |
| TestEventSubscription |      10000 |     472,348.5 ns |    208,970.61 ns |    11,454.38 ns | 242.6758 |   0.4883 |        - | 1,520,411 B |                   111 B |              111 B |
