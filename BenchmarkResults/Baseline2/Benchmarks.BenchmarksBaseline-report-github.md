``` ini

BenchmarkDotNet=v0.13.0, OS=Windows 10.0.19042.1288 (20H2/October2020Update)
Intel Core i7-10750H CPU 2.60GHz, 1 CPU, 12 logical and 6 physical cores
.NET SDK=5.0.400
  [Host]   : .NET 5.0.9 (5.0.921.35908), X64 RyuJIT  [AttachedDebugger]
  ShortRun : .NET Core 5.0.9 (CoreCLR 5.0.921.35908, CoreFX 5.0.921.35908), X64 RyuJIT

Job=ShortRun  IterationCount=3  LaunchCount=1  
WarmupCount=3  

```
|                Method | iterations |             Mean |             Error |          StdDev |    Gen 0 |    Gen 1 |    Gen 2 |   Allocated | Allocated native memory | Native memory leak |
|---------------------- |----------- |-----------------:|------------------:|----------------:|---------:|---------:|---------:|------------:|------------------------:|-------------------:|
|      **TestBitmapDrawer** |       **1000** |  **43,029,641.7 ns** |   **7,254,542.40 ns** |   **397,645.95 ns** |        **-** |        **-** |        **-** |   **176,106 B** |            **12,624,000 B** |                  **-** |
|           TestCaching |       1000 |      23,062.1 ns |       4,202.48 ns |       230.35 ns |  22.5830 |   5.6458 |        - |   142,240 B |                       - |                  - |
|    TestStaticVariable |       1000 |       5,639.5 ns |         560.63 ns |        30.73 ns |   3.8223 |        - |        - |    24,000 B |                       - |                  - |
|   TestUnmanagedMemory |       1000 |      73,553.6 ns |      14,995.43 ns |       821.95 ns |   3.7842 |        - |        - |    24,000 B |                10,000 B |                  - |
|            TestBoxing |       1000 |         245.4 ns |          67.04 ns |         3.67 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |       1000 |         248.5 ns |          12.40 ns |         0.68 ns |        - |        - |        - |           - |                       - |                  - |
| TestEventSubscription |       1000 |     849,621.8 ns |   1,273,880.95 ns |    69,825.71 ns |  27.3438 |   9.7656 |   0.4883 |   169,234 B |                   110 B |              110 B |
|      **TestBitmapDrawer** |       **5000** | **220,980,477.8 ns** |  **29,312,140.72 ns** | **1,606,697.34 ns** |        **-** |        **-** |        **-** |   **880,000 B** |            **63,120,000 B** |                  **-** |
|           TestCaching |       5000 |     206,494.1 ns |       9,729.12 ns |       533.29 ns |  90.8203 |  90.8203 |  90.8203 |   651,448 B |                       - |                  - |
|    TestStaticVariable |       5000 |      27,604.4 ns |       4,804.69 ns |       263.36 ns |  19.1040 |        - |        - |   120,000 B |                       - |                  - |
|   TestUnmanagedMemory |       5000 |     359,235.5 ns |      45,891.23 ns |     2,515.45 ns |  19.0430 |        - |        - |   120,000 B |                50,000 B |                  - |
|            TestBoxing |       5000 |       1,194.9 ns |          99.00 ns |         5.43 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |       5000 |       1,202.4 ns |          47.94 ns |         2.63 ns |        - |        - |        - |           - |                       - |                  - |
| TestEventSubscription |       5000 |   3,439,388.2 ns |  24,014,774.14 ns | 1,316,330.80 ns | 140.6250 |  46.8750 |        - |   891,764 B |                   118 B |              118 B |
|      **TestBitmapDrawer** |      **10000** | **438,916,000.0 ns** | **133,342,742.11 ns** | **7,308,965.64 ns** |        **-** |        **-** |        **-** | **1,760,144 B** |           **126,240,000 B** |                  **-** |
|           TestCaching |      10000 |     481,333.5 ns |     267,928.54 ns |    14,686.07 ns | 222.1680 | 222.1680 | 222.1680 | 1,342,032 B |                       - |                  - |
|    TestStaticVariable |      10000 |      61,923.9 ns |      35,117.12 ns |     1,924.89 ns |  38.2080 |        - |        - |   240,000 B |                       - |                  - |
|   TestUnmanagedMemory |      10000 |     721,143.8 ns |      26,938.30 ns |     1,476.58 ns |  38.0859 |        - |        - |   240,000 B |               100,000 B |                  - |
|            TestBoxing |      10000 |       2,451.7 ns |         747.52 ns |        40.97 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |      10000 |       2,405.9 ns |         245.53 ns |        13.46 ns |        - |        - |        - |           - |                       - |                  - |
| TestEventSubscription |      10000 |   6,602,969.5 ns |  35,731,131.67 ns | 1,958,543.90 ns | 257.8125 |  93.7500 |        - | 1,782,884 B |                   115 B |              115 B |
