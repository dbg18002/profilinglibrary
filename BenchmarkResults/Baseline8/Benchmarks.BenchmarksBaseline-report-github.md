``` ini

BenchmarkDotNet=v0.13.0, OS=Windows 10.0.19042.1288 (20H2/October2020Update)
Intel Core i7-10750H CPU 2.60GHz, 1 CPU, 12 logical and 6 physical cores
.NET SDK=5.0.400
  [Host]   : .NET 5.0.9 (5.0.921.35908), X64 RyuJIT  [AttachedDebugger]
  ShortRun : .NET Core 5.0.9 (CoreCLR 5.0.921.35908, CoreFX 5.0.921.35908), X64 RyuJIT

Job=ShortRun  IterationCount=3  LaunchCount=1  
WarmupCount=3  

```
|                Method | iterations |             Mean |            Error |          StdDev |    Gen 0 |    Gen 1 |    Gen 2 |   Allocated | Allocated native memory | Native memory leak |
|---------------------- |----------- |-----------------:|-----------------:|----------------:|---------:|---------:|---------:|------------:|------------------------:|-------------------:|
|      **TestBitmapDrawer** |       **1000** |  **49,341,446.7 ns** | **35,997,227.92 ns** | **1,973,129.53 ns** |        **-** |        **-** |        **-** |   **176,081 B** |            **13,183,860 B** |       **11,615,860 B** |
|           TestCaching |       1000 |     157,651.5 ns |      5,766.66 ns |       316.09 ns |  26.1230 |   6.3477 |        - |   166,243 B |                       - |                  - |
|    TestStaticVariable |       1000 |       5,189.7 ns |      3,578.31 ns |       196.14 ns |   3.8223 |        - |        - |    24,000 B |                       - |                  - |
|   TestUnmanagedMemory |       1000 |      69,013.3 ns |     16,496.64 ns |       904.24 ns |   3.7842 |        - |        - |    24,000 B |                10,000 B |                  - |
|            TestBoxing |       1000 |         237.6 ns |         23.10 ns |         1.27 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |       1000 |         242.1 ns |         49.23 ns |         2.70 ns |        - |        - |        - |           - |                       - |                  - |
| TestEventSubscription |       1000 |      52,183.4 ns |     12,542.77 ns |       687.51 ns |  24.3530 |   0.1221 |        - |   152,385 B |                   102 B |              102 B |
|      **TestBitmapDrawer** |       **5000** | **202,095,722.2 ns** | **54,985,092.69 ns** | **3,013,918.47 ns** |        **-** |        **-** |        **-** |   **880,021 B** |            **65,919,440 B** |       **58,079,440 B** |
|           TestCaching |       5000 |     935,765.7 ns |     78,961.71 ns |     4,328.16 ns |  90.8203 |  90.8203 |  90.8203 |   771,448 B |                       - |                  - |
|    TestStaticVariable |       5000 |      27,046.1 ns |     12,202.19 ns |       668.84 ns |  19.1040 |        - |        - |   120,000 B |                       - |                  - |
|   TestUnmanagedMemory |       5000 |     361,991.7 ns |    117,073.67 ns |     6,417.20 ns |  19.0430 |        - |        - |   120,000 B |                50,000 B |                  - |
|            TestBoxing |       5000 |       1,207.1 ns |        214.01 ns |        11.73 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |       5000 |       1,194.5 ns |        103.40 ns |         5.67 ns |        - |        - |        - |           - |                       - |                  - |
| TestEventSubscription |       5000 |     250,752.8 ns |     77,100.19 ns |     4,226.12 ns | 121.3379 |   0.2441 |        - |   760,420 B |                   114 B |              114 B |
|      **TestBitmapDrawer** |      **10000** | **421,120,000.0 ns** | **86,034,245.47 ns** | **4,715,827.30 ns** |        **-** |        **-** |        **-** | **1,760,144 B** |           **131,839,440 B** |      **116,159,440 B** |
|           TestCaching |      10000 |   1,906,412.2 ns |    240,543.47 ns |    13,185.00 ns | 220.7031 | 220.7031 | 220.7031 | 1,582,032 B |                       - |                  - |
|    TestStaticVariable |      10000 |      55,139.4 ns |     12,072.21 ns |       661.72 ns |  38.2080 |        - |        - |   240,000 B |                       - |                  - |
|   TestUnmanagedMemory |      10000 |     696,451.3 ns |     55,745.50 ns |     3,055.60 ns |  38.0859 |        - |        - |   240,000 B |               100,000 B |                  - |
|            TestBoxing |      10000 |       2,376.1 ns |        188.06 ns |        10.31 ns |        - |        - |        - |           - |                       - |                  - |
|          TestUnboxing |      10000 |       2,413.8 ns |        198.42 ns |        10.88 ns |        - |        - |        - |           - |                       - |                  - |
| TestEventSubscription |      10000 |     481,691.9 ns |    165,416.40 ns |     9,067.03 ns | 242.6758 |   0.4883 |        - | 1,520,414 B |                   110 B |              110 B |
