

//SETTUP FOR SOLUTION
	To be able to run the tests, the solution needs to be opened as administrator. 
	BenchmarkDotNet also need the solution to be set on Release mode to be able to work.

//RUNNING TESTS
	Benchmarks:
		To run the benchmarks you rightclick and select Run Tests on either 
		1. The ProfilingTestsFixed test class (without leaks) within the ProfilingTestFixed test project or 
		2. The ProfilingTests test class (with leaks) within the ProfilingTest test project 
		depending on wether you want to benchmark the class library with or without leaks.

		The information and result of the benchmarks can be seen in a log file created and saved at the file location:
		ProfilingTestsFixed->bin->Release->net5.0->BenchmarkDotNet.Artifacts

	ManagedMemory:
		To run test on managed memory you can rightclick and select Run Tests on either
		the ManagedMemoryFixedTests test class (without leaks) within the ProfilingTestFixed test project or
		the ManagedMemoryLeaksTests test class (with leaks) within the ProfilingTest test project
		depending on wether you want to test the class library with or without leaks.

		You can also run seperate test on specific methods by using the Test Explorer (To open it click: Test-> Test Explorer)
		in the Test Explorer navigate to:
			1. With leaks: ProfilingTest->ManagedMemory->ManagedMemoryLeaksTest and select and run the spicifik test you want to run.
			2. Wihtout leaks: ProfilingTestFixed->ManagedMemory->ManagedMemoryFixedTest and select and run the spicifik test you want to run.

		A passed test indicates there is no managed memory leak.
		A failed test indicates there is a risk of managed memory leak.

//FOLDER LAYOUT
	Benchmarks:
		In the Benchmark project you find two benchmark classes with the same BenchmarkDotNet configuration and setup.
		The difference between them is that one benchmarks methods with memory leaks and the other methods without memory leaks.
			BenchmarksFixed - The class benchmarking methods without memory leaks
			BenchmarksLeaks - The class benchmarking methods with memory leaks
	
	ManagedMemory:
		In the ManagedMemory project you find two classes with the same setup.
		The classes measures and checks wether specifik methods causes the managed memory to increase over time.
		The difference between the two classes is that one measures methods with memory leaks and the other methods without memory leaks.
			ManagedMemoryFixed - The class measuring methods without memory leaks
			ManagedMemoryLeaks - The class measuring methods with memory leaks

	ProfilingLibrary:
		Contains the classes with the methods we have focused on (with leaks)
			1. BitmapDrawer - Draws a line on a drawing surface without disposing instances of used resources.
			2. Caching - Chaches memory by populating a Dictionary with byte arrays. Will eventualy make application run out of memory.
			3. StaticVariable - Will initialize a StaticVariable object containing a static List.  
								Each initialization of a StaticVariable object will also add an element to the List. 
								The static List will never be collected by the garbage collector.
			4. UnmanagedMemory - Allocates unmaneged memory without releasing it. Unmanaged memory will never be collected by the garbage collector

	ProfilingLibraryFixed:
		Contains the classes with the methods we have focused on (without leaks)
			1. BitmapDrawerFixed - Draws a line on a drawing surface.
									All resource instances are within "using" brackets and they will therefore be disposed when leaving the scope. 
			2. CachingFixed - Chaches memory by populating a Dictionary with a WeakReference object holding a byte array. 
								The garbage collector can collect WeakReference objects that has not been used for a while.
			3. StaticVariableFixed - Will initialize a StaticVariable object containing a static List.
										Each initialization of a StaticVariable object will also add an element to the List.
										The static List will never be collected by the garbage collector.
										To avoid memory leakage, the List is cleared before adding a new element.
			4. UnmanagedMemoryFixed - Allocates unmaneged memory, which will never be collected by the garbage collector.
										By implementing IDispose the unmanaged memory will be freed when the Dispose() method is called.

	ProfilingTest:
		Contains two test classes to test the methods with leaks
			1. ManagedMemoryLeaksTest - Contains test to measure and check if managed memory increases on the methods with leaks.
			2. ProfilingTests - Contains a test to run benchmarks on the methods with leaks.

	ProfilingTest:
		Contains two test classes to test the methods without leaks
			1. ManagedMemoryFixedTest - Contains test to measure and check if managed memory increases on the methods without leaks.
			2. ProfilingTestsFixed - Contains a test to run benchmarks on the methods with leaks.