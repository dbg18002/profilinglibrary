﻿namespace ProfilingLibrary
{
    public interface IProfiling
    {
        /// <summary>
        /// Draws a line on a drawing surface without disposing instances of used resources.
        /// </summary>
        void RunBitmapDrawer();

        /// <summary>
        /// Chaches memory by populating a Dictionary with byte arrays. 
        /// Will eventualy make application run out of memory.
        /// </summary>
        /// <param name="arg">Index in Dictionary to store byte array.</param>
        void RunCaching(int arg);

        /// <summary>
        /// Will initialize a StaticVariable object containing a static List.
        /// Each initialization of a StaticVariable object will also add an element to the List.
        /// The static List will never be collected by the garbage collector.
        /// </summary>
        void RunStaticVariable();

        /// <summary>
        /// Allocates unmaneged memory without releasing it. 
        /// Unmanaged memory will never be collected by the garbage collector.
        /// </summary>
        void RunUnmanagedMemory();

        //TODO: summary
        void RunBoxing(int arg);

        //TODO: summary
        void RunUnboxing(int arg);

        //TODO: summary
        void RunEventTest(int arg);
    }
}
