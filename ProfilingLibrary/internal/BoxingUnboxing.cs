﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfilingLibrary
{
    class BoxingUnboxing
    {
        public object Boxing(int val)
        {
            return val;
        }

        public int Unboxing(object val)
        {
            return (int)val;
        }
    }
}
