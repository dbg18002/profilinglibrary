﻿

using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Timers;

namespace ProfilingLibrary
{
    internal class Profiling : IProfiling
    {
        public void RunBitmapDrawer()
        {
            BitmapDrawer bitmap = new BitmapDrawer();
            bitmap.RunBitmap();
        }

     
        public void RunCaching(int iterations)
        {
            Caching cache = new Caching();
            for (int i = 0; i < iterations; i++)
            {
                cache.Get(i);
            }
        }

        public void RunStaticVariable()
        {
            StaticVariable _static = new StaticVariable();
        }

        public void RunUnmanagedMemory()
        {
            UnmanagedMemory memory = new UnmanagedMemory();
        }

        public void RunBoxing(int iterations)
        {
            BoxingUnboxing boxing = new BoxingUnboxing();
            for (int i = 0; i < iterations; i++)
            {
                boxing.Boxing(i);
            }
        }

        public void RunUnboxing(int iterations)
        {
            BoxingUnboxing unboxing = new BoxingUnboxing();
            for (int i = 0; i < iterations; i++)
            {
                unboxing.Unboxing(i);
            }
        }

        public void RunEventTest(int iterations)
        {
            Timer myTimer = new Timer(2000);
            myTimer.Start();

            for (int i = 0; i < iterations; i++)
            {
                EventSubscriber subscribe = new EventSubscriber(myTimer);
            }
        }


    }
}
