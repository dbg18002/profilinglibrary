﻿using System.Collections.Generic;

internal class Caching
{
    private Dictionary<int, byte[]> CacheContainer { get; } = new Dictionary<int, byte[]>();

    public void Get(int id)
    {
        if (!CacheContainer.ContainsKey(id))
        {
            var instance = GetInstance(id);
            CacheContainer[id] = instance;
        }
    }

    private byte[] GetInstance(int id)
    {
        return new byte[10];
    }
}