﻿using System.Drawing;

namespace ProfilingLibrary
{
    internal class BitmapDrawer
    {
        public void RunBitmap()
        {
            var flag = new Bitmap(200, 100);
            var graphics = Graphics.FromImage(flag);
            var blackPen = new Pen(Color.Black, 3);
            graphics.DrawLine(blackPen, 100, 100, 500, 100);
        }
    }
}
