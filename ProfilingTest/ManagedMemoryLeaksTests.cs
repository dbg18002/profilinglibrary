﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ManagedMemory
{
    [TestClass]
    public class ManagedMemoryLeaksTests
    {
        [TestMethod]
        public void MeasureBitmapDrawerTest()
        {
            ManagedMemoryLeaks measure = new ManagedMemoryLeaks();
            bool memGrowth = measure.MeasureBitmapDrawer();
            Assert.AreEqual(false, memGrowth);
        }

        [TestMethod]
        public void MeasureCachingTest()
        {
            ManagedMemoryLeaks measure = new ManagedMemoryLeaks();
            bool memGrowth = measure.MeasureCaching();
            Assert.AreEqual(false, memGrowth);
        }

        [TestMethod]
        public void MeasureStaticVariableTest()
        {
            ManagedMemoryLeaks measure = new ManagedMemoryLeaks();
            bool memGrowth = measure.MeasureStaticVariable();
            Assert.AreEqual(false, memGrowth);
        }

        [TestMethod]
        public void MeasureUnmanagedMemoryTest()
        {
            ManagedMemoryLeaks measure = new ManagedMemoryLeaks();
            bool memGrowth = measure.MeasureUnmanagedMemory();
            Assert.AreEqual(false, memGrowth);
        }
    }
}