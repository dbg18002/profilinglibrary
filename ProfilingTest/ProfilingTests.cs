using Microsoft.VisualStudio.TestTools.UnitTesting;
using BenchmarkDotNet.Running;

namespace ProfilingTest
{
    [TestClass]
    public class ProfilingTests
    {
        [TestMethod]
        public void Benchmark()
        {
            BenchmarkRunner.Run<Benchmarks.BenchmarksLeaks>();
        }
    }
}
