from tkinter import *


class Footer:
    def __init__(self, frame):
        self.frame = frame
        self.frame.pack(side=BOTTOM, fill=X, expand=False)
        self.infoLabelText = StringVar()
        self.initInformation()

    def initInformation(self):
        createdByText = StringVar()
        createdByText.set("Created by: Daniel Borg, Niklas Rigelius")
        createdBy = Label(self.frame, textvariable=createdByText, bg='yellow')
        createdBy.pack(side=RIGHT, expand=False)

        self.infoLabelText.set("Status: static")
        infoLabel = Label(self.frame, textvariable=self.infoLabelText, bg='yellow')
        infoLabel.pack(side=LEFT, expand=False)

    def runningBenchmark(self):
        self.infoLabelText.set('Status: running benchmarks...')
