import json


class Benchmark():
    def __init__(self, methodname, iterations, gen0, gen1, gen2, alloc, allocunit,
                 nativealloc, nativeallocunit, nativeleak, nativeleakunit):
        self.methodName = methodname
        self.iterations = iterations
        self.gen0 = gen0
        self.gen1 = gen1
        self.gen2 = gen2
        self.alloc = alloc
        self.allocUnit = allocunit
        self.nativeAlloc = nativealloc
        self.nativeAllocUnit = nativeallocunit
        self.nativeLeak = nativeleak
        self.nativeLeakUnit = nativeleakunit


benchmarks = []

# JSON file
file = open('benchmark.json', "r")

# reading from file
benchmarkData = json.loads(file.read())

# iterating through the JSON list
for i in benchmarkData['Benchmarks']:
    # get metrics from benchmark
    methodName = i['MethodTitle']
    parameters = str(i['Parameters'])
    iterations = parameters.split("=", 1)[1]
    metrics = i['Metrics']
    gen0 = metrics[0]['Value']
    gen1 = metrics[1]['Value']
    gen2 = metrics[2]['Value']
    alloc = metrics[3]['Value']
    allocUnit = metrics[3]['Descriptor']['Unit']
    nativeAlloc = metrics[4]['Value']
    nativeAllocUnit = metrics[4]['Descriptor']['Unit']
    nativeLeak = metrics[5]['Value']
    nativeLeakUnit = metrics[5]['Descriptor']['Unit']

    # create an instance of Benchmark
    tempBenchmark = Benchmark(methodName, iterations, gen0, gen1, gen2, alloc, allocUnit,
                              nativeAlloc, nativeAllocUnit, nativeLeak, nativeLeakUnit)

    # add Benchmark to list of benchmarks
    benchmarks.append(tempBenchmark)

# close file
file.close()

# sort benchmarks by method name
benchmarks.sort(key=lambda x: x.methodName, reverse=True)

# print all benchmarks in the list
for benchmark in benchmarks:
    print(benchmark.methodName)
    print("Iterations:\t\t", benchmark.iterations)
    print("Gen 0:\t\t\t", benchmark.gen0)
    print("Gen 1:\t\t\t", benchmark.gen1)
    print("Gen 2:\t\t\t", benchmark.gen2)
    print("Alloc:\t\t\t", benchmark.alloc, benchmark.allocUnit)
    print("Alloc Native:\t", benchmark.nativeAlloc, benchmark.nativeAllocUnit)
    print("Native Leak:\t", benchmark.nativeLeak, benchmark.nativeLeakUnit, "\n")
