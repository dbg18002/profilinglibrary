from tkinter import *
from pandas import DataFrame
import loader
import matplotlib.pyplot as plt


def selectedResult(selected):
    test = selected
    benchmarkData = loader.fetchBenchmarkData(selected)
    data = {'Name': [], 'Data': []}
    for x in benchmarkData:
        if x.iterations == '1000':
            data['Name'].append(x.methodName)
            data['Data'].append(x.nativeLeak)
    dataFrame = DataFrame(data, columns=['Name', 'Data'])
    dataFrame.plot(x='Name', y='Data', kind='bar')

    plt.show()

    # Display Data

    print(test)


class CompareBenchmarks:
    def __init__(self, parentFrame):
        self.parentFrame = parentFrame
        # Top frame
        self.topFrame = Frame(parentFrame, bg='red', height=200)
        self.topFrame.pack(side=TOP, fill=X)

        self.dropDownsFrame = Frame(self.topFrame, bg='green', height=50)
        self.dropDownsFrame.pack(side=TOP, fill=X)

        self.buttonsFrame = Frame(self.topFrame, bg='grey', height=50)
        self.buttonsFrame.pack(side=BOTTOM, fill=X)
        #
        # # Diagram Frame
        # self.diagramFrame = Frame(parentFrame, bg='blue')
        # self.topFrame.pack(side=TOP, fill=Y)
        #
        self.benchmarkCollectionTitles = loader.GetBenchmarkResultTitle()
        self.DropdownFrameSetUp()
        self.ButtonsFrameSetUp()

    def DropdownFrameSetUp(self):
        mTopFrame = Frame(self.dropDownsFrame, height=10)
        mTopFrame.pack(side=TOP)
        mBotFrame = Frame(self.dropDownsFrame, height=10)
        mBotFrame.pack(side=BOTTOM)
        mLeftFrame = Frame(self.dropDownsFrame, width=20)
        mLeftFrame.pack(side=LEFT)

        # Benchmark Dropdowns
        startValue = StringVar(self.dropDownsFrame)
        startValue.set(self.benchmarkCollectionTitles[0])
        # 1
        dropDown1 = OptionMenu(self.dropDownsFrame, startValue, *self.benchmarkCollectionTitles, command=selectedResult)
        dropDown1.pack(side=LEFT)
        #Margin
        mSpaceFrame = Frame(self.dropDownsFrame, width=20)
        mSpaceFrame.pack(side=LEFT)
        # 2
        dropDown2 = OptionMenu(self.dropDownsFrame, startValue, *self.benchmarkCollectionTitles, command=selectedResult)
        dropDown2.pack(side=LEFT)

    def ButtonsFrameSetUp(self):
        mTopFrame = Frame(self.buttonsFrame, height=10)
        mTopFrame.pack(side=TOP)
        mBotFrame = Frame(self.buttonsFrame, height=10)
        mBotFrame.pack(side=BOTTOM)
        mLeftFrame = Frame(self.buttonsFrame, width=20)
        mLeftFrame.pack(side=LEFT)

        # Switch benchmarks
        btnFrame = Frame(self.buttonsFrame)
        btnFrame.pack(side=LEFT)
        benchmark1 = Button(btnFrame, width=25, text="Benchmark 1", relief=GROOVE)
        benchmark1.grid(column=0, row=0)

        benchmark1 = Button(btnFrame, width=25, text="Benchmark 2", relief=GROOVE)
        benchmark1.grid(column=1, row=0)

        benchmark1 = Button(btnFrame, width=25, text="Benchmark 3", relief=GROOVE)
        benchmark1.grid(column=2, row=0)

        benchmark1 = Button(btnFrame, width=25, text="Benchmark 4", relief=GROOVE)
        benchmark1.grid(column=3, row=0)


