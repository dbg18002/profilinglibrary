import os
import json


def GetBenchmarkResultTitle() -> object:
    parentFolder = os.path.dirname(os.getcwd())
    fullPath = parentFolder + '\\Benchmarkresults'
    jsonFiles = [file for file in os.listdir(fullPath) if file.endswith('.json')]
    return jsonFiles


def fetchBenchmarkData(fileName):
    benchmarks = []

    # JSON file
    parentFolder = os.path.dirname(os.getcwd())
    fullPath = parentFolder + '\\Benchmarkresults\\' + fileName
    file = open(fullPath, "r")

    # reading from file
    benchmarkData = json.loads(file.read())

    # iterating through the JSON list
    for i in benchmarkData['Benchmarks']:
        # get metrics from benchmark
        methodName = i['MethodTitle']
        parameters = str(i['Parameters'])
        iterations = parameters.split("=", 1)[1]
        metrics = i['Metrics']
        gen0 = metrics[0]['Value']
        gen1 = metrics[1]['Value']
        gen2 = metrics[2]['Value']
        alloc = metrics[3]['Value']
        allocUnit = metrics[3]['Descriptor']['Unit']
        nativeAlloc = metrics[4]['Value']
        nativeAllocUnit = metrics[4]['Descriptor']['Unit']
        nativeLeak = metrics[5]['Value']
        nativeLeakUnit = metrics[5]['Descriptor']['Unit']

        # create an instance of Benchmark
        tempBenchmark = Benchmark(methodName, iterations, gen0, gen1, gen2, alloc, allocUnit,
                                  nativeAlloc, nativeAllocUnit, nativeLeak, nativeLeakUnit)

        # add Benchmark to list of benchmarks
        benchmarks.append(tempBenchmark)

    # close file
    file.close()

    # sort benchmarks by method name
    benchmarks.sort(key=lambda x: x.methodName, reverse=True)

    return benchmarks


class Benchmark:
    def __init__(self, methodname, iterations, gen0, gen1, gen2, alloc, allocunit,
                 nativealloc, nativeallocunit, nativeleak, nativeleakunit):
        self.methodName = methodname
        self.iterations = iterations
        self.gen0 = gen0
        self.gen1 = gen1
        self.gen2 = gen2
        self.alloc = alloc
        self.allocUnit = allocunit
        self.nativeAlloc = nativealloc
        self.nativeAllocUnit = nativeallocunit
        self.nativeLeak = nativeleak
        self.nativeLeakUnit = nativeleakunit


class Baseline:
    def __init__(self, baseline_title, benchmarks):
        self.title = baseline_title
        self.benchmarks = benchmarks
