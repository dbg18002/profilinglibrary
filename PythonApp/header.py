from tkinter import *


def exitAction():
    exit()


class Header:
    def __init__(self, frame):
        self.frame = frame
        self.frame.pack(side=TOP, fill=BOTH)
        self.menu = Menu(self.frame.master)
        self.frame.master.config(menu=self.menu)
        self.initMenu()

    def initMenu(self):
        fileMenu = Menu(self.menu)
        fileMenu.add_command(label="Exit", command=exitAction)
        self.menu.add_cascade(label="File", menu=fileMenu)
        editMenu = Menu(self.menu)
        self.menu.add_cascade(label="Edit", menu=editMenu)
