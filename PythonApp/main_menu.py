from tkinter import *
import loader
import compare_benchmarks
import view_methodhistory
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

import test_data


def resetFrame(displayFrame):
    for widget in displayFrame.winfo_children():
        widget.destroy()


def selectedResult(selected):
    test = selected
    benchmarkData = loader.fetchBenchmarkData(selected)

    # Display Data

    print(test)


def CompareBenchmarks(displayFrame):
    resetFrame(displayFrame)
    compareBenchmarks = compare_benchmarks.CompareBenchmarks(displayFrame)

    # Option Frame
    # optionFrame = Frame(displayFrame, bg='red', height=200)
    # optionFrame.pack(side=TOP, fill=X)
    # # DropDown List benchmarksTitles
    # benchmarkCollectionTitles = loader.GetBenchmarkResultTitle()
    # startValue = StringVar(displayFrame)
    # startValue.set(benchmarkCollectionTitles[0])
    # dropDown = OptionMenu(displayFrame, startValue, *benchmarkCollectionTitles, command=selectedResult)
    # dropDown.pack(side=TOP, anchor=NW)
    return

def ViewHistory(displayFrame):
    resetFrame(displayFrame)
    view_methodhistory.ViewHistory(displayFrame)
    return

def RunBenchmarks(footer):
    footer.infoLabelText.set('Status: running benchmarks...')
    # shutil.move()
    # os.system(r'"..\BenchmarkConsoleApp\bin\Release\net5.0\BenchmarkConsoleApp.exe"')


class MainMenu:
    def __init__(self, frame, displayFrame, footer):
        self.frame = frame
        self.displayFrame = displayFrame
        self.footer = footer
        self.frame.pack(side=LEFT, fill=Y, expand=False)
        self.initMenu()

    def initMenu(self):
        # Header
        headerLabelText = StringVar()
        headerLabelText.set("Welcome!")
        headerLabel = Label(self.frame, textvariable=headerLabelText, padx=30, font=('Helvetica bold', 20))
        headerLabel.grid(column=0, row=0)
        subLabelText = StringVar()
        subLabelText.set("")
        headerLabel = Label(self.frame, textvariable=subLabelText, padx=30, font=('Helvetica bold', 10))
        headerLabel.grid(column=0, row=1)
        # Buttons
        # 1
        compareBenchmarkButton = Button(self.frame, width=25, text="Compare Benchmarks", relief=GROOVE,
                                        command=lambda: CompareBenchmarks(self.displayFrame.frame))
        compareBenchmarkButton.grid(column=0, row=2)

        #2
        StatisticsButton = Button(self.frame, width=25, text="Benchmark Statistics", relief=GROOVE)
        StatisticsButton.grid(column=0, row=3)

        #3
        runBenchmarksButton = Button(self.frame, width=25, text="Run Benchmarks", relief=GROOVE, command=lambda: RunBenchmarks(self.footer))
        runBenchmarksButton.grid(column=0, row=4)

        #4
        methodHistoryButton = Button(self.frame, width=25, text="Method History", relief=GROOVE,
                                     command=lambda: ViewHistory(self.displayFrame.frame))
        methodHistoryButton.grid(column=0, row=8)
