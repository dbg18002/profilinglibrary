from tkinter import *
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from pandas import DataFrame

import loader


def selectedResult(selected):
    test = selected
    benchmarkData = loader.fetchBenchmarkData(selected)
    data = {'Name': [], 'Data': []}
    for x in benchmarkData:
        if x.iterations == '1000':
            data['Name'].append(x.methodName)
            data['Data'].append(x.nativeLeak)
    dataFrame = DataFrame(data, columns=['Name', 'Data'])
    dataFrame.plot(x='Name', y='Data', kind='bar')

    plt.show()

    # Display Data
    print(test)


class ViewHistory:
    def __init__(self, parentFrame):
        self.parentFrame = parentFrame
        self.metric = 4
        self.methodName = 'TestEventSubscription'
        self.iterations = '10000'
        self.RenderFrame(self.metric, self.methodName)

    def RenderFrame(self, metric, methodName):
        # Clear frame:
        for widget in self.parentFrame.winfo_children():
            widget.destroy()
        self.metric = metric
        self.methodName = methodName

        # Top frame
        self.topFrame = Frame(self.parentFrame, height=200)
        self.topFrame.pack(side=TOP, fill=X)

        self.dropDownsFrame = Frame(self.topFrame, height=50)
        self.dropDownsFrame.pack(side=TOP, fill=X, pady=(10, 5))

        self.buttonsFrame = Frame(self.topFrame, height=50)
        self.buttonsFrame.pack(side=BOTTOM, fill=X, pady=(5, 10))

        self.benchmarkCollectionTitles = loader.GetBenchmarkResultTitle()

        self.displayDiagram()
        self.DropdownFrameSetUp()
        self.ButtonsFrameSetUp()

    def DropdownFrameSetUp(self):
        # Benchmark Dropdowns
        startValue = StringVar(self.dropDownsFrame)
        methodNames = []
        benchmarks = loader.fetchBenchmarkData(self.benchmarkCollectionTitles[0])

        for benchmark in benchmarks:
            if benchmark.methodName not in methodNames:
                methodNames.append(benchmark.methodName)

        startValue.set(methodNames[0])

        # Centered frame for Method selector and label
        methodSelectorFrame = Frame(self.dropDownsFrame)
        methodSelectorFrame.pack(side=BOTTOM)

        # Label for method selection
        methodSelectorLabel = Label(methodSelectorFrame, text="Select Benchmarked method:")
        methodSelectorLabel.pack(side=LEFT)

        # Method selector dropdown
        methodSelector = OptionMenu(methodSelectorFrame, startValue, *methodNames)
        methodSelector.pack(side=LEFT)
        methodSelector.config(width=34, anchor='w')


    def ButtonsFrameSetUp(self):
        # Metrics buttons
        btnFrame = Frame(self.buttonsFrame)
        btnFrame.pack(side=BOTTOM, expand=True)

        gen0Button = Button(btnFrame, width=15, text="Gen 0", relief=GROOVE, command=lambda: self.RenderFrame(0, self.methodName))
        gen0Button.grid(column=0, row=0)

        gen1Button = Button(btnFrame, width=15, text="Gen 1", relief=GROOVE, command=lambda: self.RenderFrame(1, self.methodName))
        gen1Button.grid(column=1, row=0)

        gen2Button = Button(btnFrame, width=15, text="Gen 2", relief=GROOVE, command=lambda: self.RenderFrame(2, self.methodName))
        gen2Button.grid(column=2, row=0)

        allocButton = Button(btnFrame, width=15, text="Allocated", relief=GROOVE, command=lambda: self.RenderFrame(3, self.methodName))
        allocButton.grid(column=3, row=0)

        nativeAllocButton = Button(btnFrame, width=15, text="Native Allocated", relief=GROOVE, command=lambda: self.RenderFrame(4, self.methodName))
        nativeAllocButton.grid(column=4, row=0)

        nativeLeakButton = Button(btnFrame, width=15, text="Native Leak", relief=GROOVE, command=lambda: self.RenderFrame(5, self.methodName))
        nativeLeakButton.grid(column=5, row=0)

    def displayDiagram(self):
        baselines = []
        titles = []
        values = []
        unit = None
        header = ""

        # Fetch all available baselines:
        for title in self.benchmarkCollectionTitles:
            baselines.append(loader.Baseline(title, loader.fetchBenchmarkData(title)))

        # For each baseline, save the title and wanted values
        for baseline in baselines:
            titles.append(baseline.title[42:61])

            # Find method in baseline and filter data:
            for result in baseline.benchmarks:
                if result.methodName == self.methodName:
                    if result.iterations == self.iterations:
                        if self.metric == 0:
                            values.append(result.gen0)
                            header = "Gen 0 Collections"
                        elif self.metric == 1:
                            values.append(result.gen1)
                            header = "Gen 1 Collections"
                        elif self.metric == 2:
                            values.append(result.gen2)
                            header = "Gen 2 Collections"
                        elif self.metric == 3:
                            values.append(result.alloc)
                            unit = result.allocUnit
                            header = "Allocated Memory"
                        elif self.metric == 4:
                            values.append(result.nativeAlloc)
                            unit = result.nativeAllocUnit
                            header = "Allocated Native Memory"
                        elif self.metric == 5:
                            values.append(result.nativeLeak)
                            unit = result.nativeLeakUnit
                            header = "Native Memory Leak"

        dataFrame = self.createFrame(titles, values)
        self.renderDiagram(dataFrame, header, unit)

    def createFrame(self, titles, values):
        resultData = {'Benchmark': titles, 'Value': values}
        resultDataFrame = DataFrame(resultData, columns=['Benchmark', 'Value'])
        return resultDataFrame

    def renderDiagram(self, dataframe, title, unit=None):
        resultFigure = plt.Figure(figsize=(10.2, 14))

        # To avoid benchmark names from clipping
        resultFigure.subplots_adjust(bottom=0.28)

        ax = resultFigure.add_subplot()
        bar1 = FigureCanvasTkAgg(resultFigure, self.parentFrame)
        bar1.get_tk_widget().pack()
        df1 = dataframe[['Benchmark', 'Value']].groupby('Benchmark').sum()
        df1.plot(kind='bar', legend=True, ax=ax)

        # Add label at bottom with some padding
        ax.set_xlabel('Benchmarks by date', labelpad=20)

        if unit is not None:
            ax.set_title(title + " (" + unit + ")")
        else:
            ax.set_title(title)

