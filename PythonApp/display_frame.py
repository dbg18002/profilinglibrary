from tkinter import *


class DisplayFrame:
    def __init__(self, frame):
        self.frame = frame
        self.frame.pack(side=RIGHT, fill=BOTH, expand=True)
