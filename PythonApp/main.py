from tkinter import *

import footer
import header
import main_menu
import display_frame

# ----------- tkinter -------------
root = Tk()

# // FRAME Declaration //
_header = header.Header(Frame(root))

_footer = footer.Footer(Frame(root, height=25, bg='yellow'))

_displayFrame = display_frame.DisplayFrame(Frame(root, bg='#597678'))

_mainMenu = main_menu.MainMenu(Frame(root, width=200), _displayFrame, _footer)


root.wm_title("Benchmark Statistics Application")
root.geometry("1200x900")
root.mainloop()
