from pandas import DataFrame


def CountryData():
    countryData = {'Country': ['US', 'CA', 'GER', 'UK', 'FR'],
                   'GDP Per Capita': [45000, 42000, 52000, 49000, 47000]}
    countryDataFrame = DataFrame(countryData, columns=['Country', 'GDP Per Capita'])
    return countryDataFrame


def UnemploymentRateData():
    unemploymentRateData = {'Year': [1920, 1930, 1940, 1950, 1960, 1970, 1980, 1990, 2000, 2010],
                            'Unemployment Rate': [9.8, 12, 8, 7.2, 6.9, 7, 6.5, 6.2, 5.5, 6.3]}
    unemploymentRateDataFrame = DataFrame(unemploymentRateData, columns=['Year', 'Unemployment Rate'])
    return unemploymentRateDataFrame


def InterestRateData():
    interestRateData = {'Interest Rate': [5, 5.5, 6, 5.5, 5.25, 6.5, 7, 8, 7.5, 8.5],
                        'Stock Index Price': [1500, 1520, 1525, 1523, 1515, 1540, 1545, 1560, 1555, 1565]}
    interestRateDateFrame = DataFrame(interestRateData, columns=['Interest Rate', 'Stock Index Price'])
    return interestRateDateFrame
