﻿namespace ProfilingLibraryFixed
{
    public interface IProfilingFixed
    {
        /// <summary>
        /// Draws a line on a drawing surface.
        /// All resource instances are within "using" brackets and they will therefore
        /// be disposed when leaving the scope. 
        /// </summary>
        void RunBitmapDrawer();

        /// <summary>
        /// Chaches memory by populating a Dictionary with a WeakReference object holding a byte array. 
        /// The garbage collector can collect WeakReference objects that has not been used for a while.
        /// </summary>
        /// <param name="arg">Index in Dictionary to store byte array.</param>
        void RunCaching(int arg);

        /// <summary>
        /// Will initialize a StaticVariable object containing a static List.
        /// Each initialization of a StaticVariable object will also add an element to the List.
        /// The static List will never be collected by the garbage collector.
        /// To avoid memory leakage, the List is cleared before adding a new element.
        /// </summary>
        void RunStaticVariable();

        /// <summary>
        /// Allocates unmaneged memory, which will never be collected by the garbage collector.
        /// By implementing IDispose the unmanaged memory will be freed when the Dispose() method is called.
        /// </summary>
        void RunUnmanagedMemory();

        //TODO: summary
        void RunNoBoxing(int arg);

        //TODO: summary
        void RunNoUnboxing(int arg);

        //TODO: summary
        void RunEventWithUnsub(int arg);
    }
}
