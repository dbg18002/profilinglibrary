﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ProfilingLibraryFixed
{
    class EventSubscriberFixed
    {
        public EventSubscriberFixed(Timer timer)
        {
            timer.Elapsed += WriteToConsole;
        }

        public void UnsubscribeToEvent(Timer timer)
        {
            timer.Elapsed -= WriteToConsole;
        }

        private void WriteToConsole(object sender, EventArgs e)
        {

        }
    }
}
