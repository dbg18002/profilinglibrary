﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProfilingLibraryFixed
{
    class NoBoxingUnboxing
    {
        public int NoBoxing(int val)
        {
            return val;
        }

        public int NoUnboxing(int val)
        {
            return val;
        }
    }
}
