﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Timers;

namespace ProfilingLibraryFixed
{
    internal class ProfilingFixed : IProfilingFixed
    {
        public void RunBitmapDrawer()
        {
            BitmapDrawerFixed bitmap = new BitmapDrawerFixed();
            bitmap.RunBitmap();
        }

        public void RunCaching(int iterations)
        {
            CacheFixed cache = new CacheFixed();
            for (int i = 0; i < iterations; i++)
            {
                cache.Get(i);
            }
        }

        public void RunStaticVariable()
        {
            StaticVariableFixed _static = new StaticVariableFixed();
        }

        public void RunUnmanagedMemory()
        {
            UnmanagedMemoryFixed memory = new UnmanagedMemoryFixed();
            memory.Dispose();
        }

        public void RunNoBoxing(int iterations)
        {
            NoBoxingUnboxing noBoxing = new NoBoxingUnboxing();
            for (int i = 0; i < iterations; i++)
            {
                noBoxing.NoBoxing(i);
            }
        }     
        
        public void RunNoUnboxing(int iterations)
        {
            NoBoxingUnboxing noUnboxing = new NoBoxingUnboxing();
            for (int i = 0; i < iterations; i++)
            {
                noUnboxing.NoUnboxing(i);
            }
        }

        public void RunEventWithUnsub(int iterations)
        {
            Timer myTimer = new Timer(2000);
            myTimer.Start();

            for (int i = 0; i < iterations; i++)
            {
                EventSubscriberFixed subscribe = new EventSubscriberFixed(myTimer);
                subscribe.UnsubscribeToEvent(myTimer);
            }
        }
    }
}
