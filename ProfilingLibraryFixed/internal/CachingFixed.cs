﻿using System;
using System.Collections.Generic;

internal class CacheFixed
{
    private Dictionary<int, WeakReference> CacheContainer { get; } = new Dictionary<int, WeakReference>();

    public void Get(int id)
    {
        if (!CacheContainer.ContainsKey(id))
        {
            var instance = GetInstance(id);
            CacheContainer[id] = new WeakReference(instance, false);
        }
    }

    private byte[] GetInstance(int id)
    {
        return new byte[10];
    }
}