﻿using System.Drawing;

namespace ProfilingLibraryFixed
{
    class BitmapDrawerFixed
    {
        public void RunBitmap()
        {
            using (var flag = new Bitmap(200, 100))
            {
                using (var graphics = Graphics.FromImage(flag))
                {
                    using (var blackPen = new Pen(Color.Black, 3))
                    {
                        graphics.DrawLine(blackPen, 100, 100, 500, 100);
                    }
                }
            }
        }
    }
}
