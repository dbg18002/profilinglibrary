﻿using System;
using System.Runtime.InteropServices;

internal class UnmanagedMemoryFixed : IDisposable
{
    private IntPtr buffer;

    public UnmanagedMemoryFixed()
    {
        buffer = Marshal.AllocHGlobal(10);
    }

    public void Dispose()
    {
        Marshal.FreeHGlobal(buffer);
    }
}
