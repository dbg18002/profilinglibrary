﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnostics.Windows.Configs;
using BenchmarkDotNet.Engines;
using ProfilingLibrary;
using System;
using System.Collections.Generic;

namespace Benchmarks
{
    //FILE LOCATION
    //ProfilingTests->bin->Release->net5.0->BenchmarkDotNet.Artifacts
    //[SimpleJob(RunStrategy.Monitoring, launchCount: 1, warmupCount: 3, targetCount: 3, invocationCount: 1)]
    [NativeMemoryProfiler]
    [MemoryDiagnoser]   
    [ShortRunJob]
    [JsonExporterAttribute.Full()]
    public class BenchmarksLeaks
    {
        IProfiling profiling = ProfilingLibraryFactory.GetProfilingLibrary();

        [Params(10, 50, 100)]
        public int iterations { get; set; }

        [Benchmark]
        public void BitmapDrawerWithLeaks()
        {
            for (int i = 0; i < iterations; i++)
            {
                profiling.RunBitmapDrawer();
            }
        }

        [Benchmark]
        public void TestCaching()
        {
            profiling.RunCaching(iterations);
        }

        [Benchmark]
        public void TestStaticVariable()
        {
            for (int i = 0; i < iterations; i++)
            {
                profiling.RunStaticVariable();
            }
        }

        [Benchmark]
        public void TestUnmanagedMemory()
        {
            for (int i = 0; i < iterations; i++)
            {
                profiling.RunUnmanagedMemory();
            }
        }

        [Benchmark]
        public void TestBoxing()
        {
            profiling.RunBoxing(iterations);
        }

        [Benchmark]
        public void TestUnboxing()
        {
            profiling.RunUnboxing(iterations);
        }

        [Benchmark]
        public void TestEventsSubscription()
        {
            profiling.RunEventTest(iterations);
        }
    }
}
