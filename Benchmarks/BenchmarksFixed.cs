﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnostics.Windows.Configs;
using BenchmarkDotNet.Engines;
using ProfilingLibraryFixed;
using System;

namespace Benchmarks
{
    //FILE LOCATION
    //ProfilingTestsFixed->bin->Release->net5.0->BenchmarkDotNet.Artifacts

    [ShortRunJob]
    [NativeMemoryProfiler]
    [MemoryDiagnoser]
    [JsonExporterAttribute.Full()]
    public class BenchmarksFixed
    {
        IProfilingFixed profiling = ProfilingLibraryFixedFactory.GetProfilingLibrary();

        [Params(10, 50, 100)]
        public int iterations { get; set; }

        [Benchmark]
        public void BitmapDrawerFixed()
        {
            for (int i = 0; i < iterations; i++)
            {
                profiling.RunBitmapDrawer();
            }

        }

        [Benchmark]
        public void TestChachingFixed()
        {
            profiling.RunCaching(iterations);
        }

        [Benchmark]
        public void TestStaticVariableFixed()
        {
            for (int i = 0; i < iterations; i++)
            {
                profiling.RunStaticVariable();
            }

        }

        [Benchmark]
        public void TestUnmanagedMemoryFixed()
        {
            for (int i = 0; i < iterations; i++)
            {
                profiling.RunUnmanagedMemory();
            }

        }

        [Benchmark]
        public void TestNoBoxing()
        {
            profiling.RunNoBoxing(iterations);
        }

        [Benchmark]
        public void TestNoUnboxing()
        {
            profiling.RunNoUnboxing(iterations);
        }

        [Benchmark]
        public void TestEventsWithUnsub()
        {
            profiling.RunEventWithUnsub(iterations);
        }
    }
}
