﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Diagnostics.Windows.Configs;
using BenchmarkDotNet.Engines;
using ProfilingLibraryFixed;
using ProfilingLibrary;
using System;
using System.IO;

namespace Benchmarks
{
    //FILE LOCATION
    //ProfilingTestsFixed->bin->Release->net5.0->BenchmarkDotNet.Artifacts

    [ShortRunJob]
    [NativeMemoryProfiler]
    [MemoryDiagnoser]
    [JsonExporterAttribute.Full()]
    public class BenchmarksBaseline
    {
        IProfilingFixed profilingGood = ProfilingLibraryFixedFactory.GetProfilingLibrary();
        IProfiling profilingBad = ProfilingLibraryFactory.GetProfilingLibrary();

        [Params(1000, 5000, 10000)]
        public int iterations { get; set; }

        [Benchmark]
        public void TestBitmapDrawer()
        {
            for (int i = 0; i < iterations; i++)
            {
                profilingBad.RunBitmapDrawer();
                //profilingGood.RunBitmapDrawer();
            }
        }

        [Benchmark]
        public void TestCaching()
        {
            //profilingBad.RunCaching(iterations);
            profilingGood.RunCaching(iterations);
        }

        [Benchmark]
        public void TestStaticVariable()
        {
            for (int i = 0; i < iterations; i++)
            {
                //profilingBad.RunStaticVariable();
                profilingGood.RunStaticVariable();

            }
        }

        [Benchmark]
        public void TestUnmanagedMemory()
        {
                for (int i = 0; i < iterations; i++)
                {
                    //profilingBad.RunUnmanagedMemory();
                    profilingGood.RunUnmanagedMemory();

                }
         }

        [Benchmark]
        public void TestBoxing()
        {
            //profilingBad.RunBoxing(iterations);
            profilingGood.RunNoBoxing(iterations);
        }

        [Benchmark]
        public void TestUnboxing()
        {
            profilingBad.RunUnboxing(iterations);
            //profilingGood.RunNoUnboxing(iterations);
        }

        [Benchmark]
        public void TestEventSubscription()
        {
            profilingBad.RunEventTest(iterations);
            //profilingGood.RunEventWithUnsub(iterations);
        }
    }
}
