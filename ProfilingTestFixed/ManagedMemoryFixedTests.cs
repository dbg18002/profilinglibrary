﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ManagedMemory
{
    [TestClass]
    public class ManagedMemoryFixedTests
    {
        [TestMethod]
        public void MeasureBitmapDrawerTest()
        {
            ManagedMemoryFixed measure = new ManagedMemoryFixed();
            bool memGrowth = measure.MeasureBitmapDrawer();
            Assert.AreEqual(false, memGrowth);
        }

        [TestMethod]
        public void MeasureCachingTest()
        {
            ManagedMemoryFixed measure = new ManagedMemoryFixed();
            bool memGrowth = measure.MeasureCaching();
            Assert.AreEqual(false, memGrowth);
        }

        [TestMethod]
        public void MeasureStaticVariableTest()
        {
            ManagedMemoryFixed measure = new ManagedMemoryFixed();
            bool memGrowth = measure.MeasureStaticVariable();
            Assert.AreEqual(false, memGrowth);
        }

        [TestMethod]
        public void MeasureUnmanagedMemoryTest()
        {
            ManagedMemoryFixed measure = new ManagedMemoryFixed();
            bool memGrowth = measure.MeasureUnmanagedMemory();
            Assert.AreEqual(false, memGrowth);
        }
    }
}