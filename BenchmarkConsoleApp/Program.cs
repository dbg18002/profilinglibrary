﻿using System;
using System.IO;
using BenchmarkDotNet.Running;

namespace BenchmarkConsoleApp
{
    class Program
    {
        static int Main(string[] args)
        {
            Console.WriteLine("What is happening");
            BenchmarkRunner.Run<Benchmarks.BenchmarksBaseline>();

            //Move File
            string benchmarkBaseLine = "Benchmarks.BenchmarksBaseline-report-full.json";
            string sourcePath = @"BenchmarkDotNet.Artifacts\results";
            string targetPath = @"..\..\..\..\BenchmarkResults";

            string sourceFile = System.IO.Path.Combine(sourcePath, benchmarkBaseLine);
            benchmarkBaseLine = String.Format(@"Benchmarks.BenchmarksBaseline-report-full-{0}.json", DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss"));
            string destinationFile = System.IO.Path.Combine(targetPath, benchmarkBaseLine);
            Console.WriteLine(destinationFile);
            System.IO.File.Move(sourceFile, destinationFile, true);


            //BenchmarkRunner.Run<Benchmarks.BenchmarksFixed>();
            //BenchmarkRunner.Run<Benchmarks.BenchmarksLeaks>();
            //ProfilingLibrary.IProfiling profiler = ProfilingLibrary.ProfilingLibraryFactory.GetProfilingLibrary();
            //profiler.RunEventTest(10);
            //Console.ReadLine();
            Console.WriteLine("What is happening");
           

            return 1;
        }
    }
}
